// Place your application-specific JavaScript functions and classes here
// This file is automatically included by javascript_include_tag :defaults
if (typeof Application == 'undefined')
	Application = {};

$(document).ready(function() {
	Application.init();
});

$.metaCsrfToken;
$.metaCsrfHeader;


Application = {

	init : function() {
		
		/* Csrf initialization */
        $.metaCsrfToken = $("meta[name='_csrf']").attr("content");
        $.metaCsrfHeader = $("meta[name='_csrf_header']").attr("content");


		if ($('#20170821154000').length != 0) {
			$('#20170821154000').click(function() {
				$("#logoutForm").submit();
			});
		}
	}
};
