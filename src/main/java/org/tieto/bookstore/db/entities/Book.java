package org.tieto.bookstore.db.entities;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Arrays;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import org.apache.commons.lang3.StringUtils;

@Entity
@Table(name = "books", catalog = "bookstore", uniqueConstraints = { @UniqueConstraint(columnNames = { "book_name" }) })
public class Book implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private String bookName;
	private String bookISBN;
	private String bookPrice;
	
	private String searchString;


	public Book() {
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id")
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "book_name", nullable = false)
	public String getBookName() {
		return bookName;
	}

	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

	@Column(name = "book_isbn", nullable = false)
	public String getBookISBN() {
		return bookISBN;
	}

	public void setBookISBN(String bookISBN) {
		this.bookISBN = bookISBN;
	}

	@Column(name = "book_price", nullable = true)
	public String getBookPrice() {
		return bookPrice;
	}

	public void setBookPrice(String bookPrice) {
		this.bookPrice = bookPrice;
	}

	@Transient
	public String getLocationAsString() {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(this.bookName != null ? this.bookName + " " : "");
		stringBuffer.append(this.bookISBN != null ? this.bookISBN + " " : "");
		stringBuffer.append(this.bookPrice != null ? this.bookPrice + " " : "");

		return stringBuffer.toString();
	}

	@Column(name = "search_string", length = 1001)
	public String getSearchString() {
		return searchString;
	}

	public void setSearchString(String searchString) {
		this.searchString = searchString;
	}

	@PreUpdate
	@PrePersist
	void updateSearchString() {
/*		if(this.atmStatusType == null){
			this.atmStatusType = AtmStatusType.UNDEFINED;
		}*/
		
		final String fullSearchString = StringUtils.join(Arrays.asList(getBookName(), getBookISBN(), getBookPrice()), " ");
		this.searchString = StringUtils.substring(fullSearchString, 0, 1000);
	}


	
	@Transient
	public String getTextStatusClass() {
/*		if(atmStatusType != null && atmStatusType.value().equals(AtmStatusType.ERROR.value())){
			return "text-danger";
		}
		else if(atmStatusType != null && atmStatusType.value().equals(AtmStatusType.WARNING.value())){
			return "text-warning";
		}
		else if(atmStatusType != null && atmStatusType.value().equals(AtmStatusType.OK.value())){
			return "text-success";
		}
		else if(atmStatusType != null && atmStatusType.value().equals(AtmStatusType.POWEROFF.value())){
			return "text-poweroff";
		}
		else {
			return "text-default";
		}*/
		
		return "text-default";
	}



}
