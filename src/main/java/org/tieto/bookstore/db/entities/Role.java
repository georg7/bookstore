package org.tieto.bookstore.db.entities;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Set;

@Entity
@Table(name = "roles", catalog = "bookstore")
public class Role {
    private Long id;
    private String name;
    private Set<User> users;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id")
	public Long  getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @ManyToMany(mappedBy = "roles")
    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }
    
    public boolean equals(Object o) {
    	return (o instanceof Role) && ((Role)o).getName().equals(this.getName());
    }

    public int hashCode() {
    	return this.getName() != null ? name.hashCode() : 0;
    }
}
