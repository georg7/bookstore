package org.tieto.bookstore.db.entities;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table(name = "side_menu", catalog = "bookstore")
public class SideMenu implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private String name;
	private String url;
	private boolean isActive;
	private TopMenu topMenu;
	private int sort;
	
	private String attrName;
	private String attrValue;
	
	public SideMenu() {
	}


	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id")
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "name", length = 255)
	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "url", length = 255)
	public String getUrl() {
		return url;
	}


	public void setUrl(String url) {
		this.url = url;
	}

	@ManyToOne(cascade=CascadeType.ALL)
    @JoinColumn(name = "top_menu_id")
	public TopMenu getTopMenu() {
		return topMenu;
	}


	public void setTopMenu(TopMenu topMenu) {
		this.topMenu = topMenu;
	}
	
	@Transient
	public boolean isActive() {
		return isActive;
	}


	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	
	@Column(name = "attr_name", length = 255)
	public String getAttrName() {
		return attrName;
	}


	public void setAttrName(String attrName) {
		this.attrName = attrName;
	}

	@Column(name = "attr_value", length = 255)
	public String getAttrValue() {
		return attrValue;
	}


	public void setAttrValue(String attrValue) {
		this.attrValue = attrValue;
	}


	public int getSort() {
		return sort;
	}


	public void setSort(int sort) {
		this.sort = sort;
	}
	
}

