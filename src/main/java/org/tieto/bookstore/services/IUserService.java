package org.tieto.bookstore.services;

import java.util.List;

import org.tieto.bookstore.db.entities.Role;
import org.tieto.bookstore.db.entities.User;

public interface IUserService {
    void save(User user);
    User findOne(Long id);
    List<User> findAll();
    User findByEmail(String email);
    void delete(Long id);
    
    List<Role> findRolesAll();
    Role findRoleOne(Long id);
   
}
