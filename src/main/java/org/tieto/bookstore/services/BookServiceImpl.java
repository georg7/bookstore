package org.tieto.bookstore.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.tieto.bookstore.db.entities.Book;
import org.tieto.bookstore.repositories.BookRepository;

@Service
public class BookServiceImpl implements IBookService {

	@Autowired
	private BookRepository bookRepository;
	
    @Override
    public void save(Book atm) {
    	bookRepository.save(atm);
    }

	@Override
	public Page<Book> findAll(Pageable pageable) {
		Book filterAtm = new Book();
		Example<Book> example = Example.of(filterAtm);
		Page<Book> bookPage = bookRepository.findAll(example, pageable);
		
		return bookPage;
	}

	@Override
	public Book findOne(Long id) {
		return bookRepository.findOne(id);
	}

	@Override
	public List<Book> findAllByName(String bookName) {
		return bookRepository.findByBookName(bookName);
	}

	@Override
	public Long countAllBooks() {
		return bookRepository.count();
	}

	@Override
	public void delete(Long id) {
		bookRepository.delete(id);
	}

	
}
