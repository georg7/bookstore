package org.tieto.bookstore.services;

import java.util.List;

import org.tieto.bookstore.db.entities.Book;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface IBookService {
	void save(Book atm);
	Page<Book> findAll(Pageable pageable);
	Book findOne(Long id);
	List<Book> findAllByName(String bookName);
	
	Long countAllBooks();
	void delete(Long id);
}
