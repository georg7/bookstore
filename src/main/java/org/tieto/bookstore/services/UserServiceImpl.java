package org.tieto.bookstore.services;

import org.tieto.bookstore.db.entities.Role;
import org.tieto.bookstore.db.entities.User;
import org.tieto.bookstore.repositories.RoleRepository;
import org.tieto.bookstore.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class UserServiceImpl implements IUserService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
  

    @Override
    public void save(User user) {
    	if(user.getId() == null || 
				!user.getPassword().equals(findOne(user.getId()).getPassword())) {
    		 user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
    	}
        userRepository.save(user);
    }
    
    @Override
    public User findOne(Long id) {
      return userRepository.findOne(id);
    }

    @Override
    public User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }
    
    @Override
    public List<User> findAll() {
        return userRepository.findAll();	
    } 

	@Override
	public void delete(Long id) {
		userRepository.delete(id);
	} 
	
    @Override
    public List<Role> findRolesAll() {
        return roleRepository.findAll();	
    }
    
    @Override
    public Role findRoleOne(Long id) {
        return roleRepository.findOne(id);	
    }
}
