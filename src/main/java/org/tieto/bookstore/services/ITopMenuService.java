package org.tieto.bookstore.services;

import org.tieto.bookstore.db.entities.TopMenu;


public interface ITopMenuService {
    Iterable<TopMenu> listAllTopMenus();
    TopMenu findByUrl(String url);
}
