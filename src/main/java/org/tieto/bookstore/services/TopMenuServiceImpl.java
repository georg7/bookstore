package org.tieto.bookstore.services;

import org.tieto.bookstore.db.entities.TopMenu;
import org.tieto.bookstore.repositories.TopMenuRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class TopMenuServiceImpl implements ITopMenuService {
    private TopMenuRepository topMenuRepository;

    @Autowired
    public void setTopMenuRepository(TopMenuRepository topMenuRepository) {
        this.topMenuRepository = topMenuRepository;
    }

    @Override
    public Iterable<TopMenu> listAllTopMenus() {
        return topMenuRepository.findAllByOrderBySortAsc();
    }
    
    @Override
    public TopMenu findByUrl(String url) {
        return topMenuRepository.findByUrl(url);
    }
    
/*    @Override
    public Set<SideMenu> listAllTSubMenus(int top_index) {
    	Set<SideMenu> sideMenu = null;
    	Iterable<TopMenu> topMenuIterable = topMenuRepository.findAll();
    	Iterator<TopMenu> iterator = topMenuIterable.iterator();
    	while(iterator.hasNext()) {
    		TopMenu topMenu = iterator.next();
    		sideMenu = topMenu.getSubmenus();
    		break;
         }
    	return sideMenu;
    }*/
}
