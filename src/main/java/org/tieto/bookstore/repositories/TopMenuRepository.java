package org.tieto.bookstore.repositories;

import org.tieto.bookstore.db.entities.TopMenu;
import org.springframework.data.jpa.repository.JpaRepository;


public interface TopMenuRepository extends JpaRepository<TopMenu, Integer>{

	Iterable<TopMenu> findAllByOrderBySortAsc();
	TopMenu findByUrl(String url);
}