package org.tieto.bookstore.repositories;

import java.util.List;

import org.tieto.bookstore.db.entities.Role;
import org.tieto.bookstore.db.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long>{
	List<Role> findAll();
	Role findOne(Long id);
	Role findByName(String name);
	
}
