package org.tieto.bookstore.repositories;

import org.tieto.bookstore.db.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
	 User findByEmail(String email);
}
