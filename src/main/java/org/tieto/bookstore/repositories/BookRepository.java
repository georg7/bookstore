package org.tieto.bookstore.repositories;

import java.util.List;

import org.tieto.bookstore.db.entities.Book;
import org.springframework.data.jpa.repository.JpaRepository;


public interface BookRepository extends JpaRepository<Book, Long> {

	List<Book> findByBookName(String bookName);
}