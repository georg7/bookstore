package org.tieto.bookstore.interceptors;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.tieto.bookstore.annotations.Link;
import org.tieto.bookstore.db.entities.SideMenu;
import org.tieto.bookstore.db.entities.TopMenu;
import org.tieto.bookstore.services.ITopMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class TopMenuInterceptor extends HandlerInterceptorAdapter {

	private static final String TOP_MENUS = "topMenus";
	private static final String SIDE_MENUS = "sideMenus";
	private static final String ACTIVE_TOP_MENU = "activeTopMenu";
	private static final String ACTIVE_SIDE_MENU = "activeSideMenu";

	private ITopMenuService topMenuService;

	@Autowired
	public void setTopMenuService(ITopMenuService topMenuService) {
		this.topMenuService = topMenuService;
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {

		Annotation[] declaredAnnotations = getDeclaredAnnotationsForHandler(handler);
		HttpSession session = request.getSession();
		for (Annotation annotation : declaredAnnotations) {
			if (annotation.annotationType().equals(Link.class)) {
				processAnnotation(request, session, annotation);
			}
		}

		return true;
	}

	private void processAnnotation(HttpServletRequest request, HttpSession session, Annotation annotation) {
		Iterable<TopMenu> topMenus = getTopMenuFromSession(session);

		if (topMenus == null) {
			topMenus = topMenuService.listAllTopMenus();
			session.setAttribute(TOP_MENUS, topMenus);
		}

		// Remove some session attributes
		//session.removeAttribute(SIDE_MENUS);
		//session.removeAttribute(ACTIVE_TOP_MENU);
		//session.removeAttribute(ACTIVE_SIDE_MENU);
		
		boolean flag = false;

		Iterator<TopMenu> iterator = topMenus.iterator();
		while (iterator.hasNext()) {
			TopMenu topMenu = iterator.next();
			Set<SideMenu> sideMenus = topMenu.getSubmenus();
			for (SideMenu sideMenu : sideMenus) {
				if (sideMenu.getUrl().equals(request.getRequestURI())) {
					session.setAttribute(ACTIVE_TOP_MENU, topMenu);
					session.setAttribute(ACTIVE_SIDE_MENU, sideMenu);
					session.setAttribute(SIDE_MENUS, sideMenus);
					flag = true;
					break;
				}
			}
			if(flag) {
				break;
			}
		}

	}

	@SuppressWarnings("unchecked")
	private Iterable<TopMenu> getTopMenuFromSession(HttpSession session) {
		Iterable<TopMenu> topMenu = (Iterable<TopMenu>) session.getAttribute(TOP_MENUS);
		return topMenu;
	}

	private Annotation[] getDeclaredAnnotationsForHandler(Object handler) {
		HandlerMethod handlerMethod = (HandlerMethod) handler;
		Method method = handlerMethod.getMethod();
		Annotation[] declaredAnnotations = method.getDeclaredAnnotations();
		return declaredAnnotations;
	}

}
