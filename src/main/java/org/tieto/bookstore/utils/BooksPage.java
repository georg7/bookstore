package org.tieto.bookstore.utils;

/*import org.tieto.bookstore.db.entities.Atm;
import org.btg.bitcoin.atm.db.entities.AtmStatusType;
import org.btg.bitcoin.atm.db.entities.CommonStatusType;
import org.btg.bitcoin.atm.services.IAtmService;*/
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import org.tieto.bookstore.db.entities.Book;
import org.tieto.bookstore.services.IBookService;

@Component
public class BooksPage {
	
	private Integer PAGE_SIZE = 50;
	private IBookService atmService;
	
	Pageable pageable = new PageRequest(0, PAGE_SIZE, Sort.Direction.DESC, "id");
	
    @Autowired
    public void setAtmService(IBookService atmService) {
        this.atmService = atmService;
    }
	
	
	public Long getCountOfAllBooks() {
		return atmService.countAllBooks();
	}

	public Pageable getPageble() {
		return pageable;
	}

	public Page<Book> getBookPage() {
		return atmService.findAll(pageable);
	}
	
	public void nextPage() {
		pageable = pageable.next();
	}
	
	public void previousPage() {
		pageable = pageable.previousOrFirst();
	}
	
	public String getPageSizeInfo() {
		Long countOfAll = getCountOfAllBooks();
		if(countOfAll < PAGE_SIZE) {
			return String.valueOf(countOfAll);
		}
		int countOfVieved = pageable.getOffset() + pageable.getPageSize();
		if(countOfVieved > countOfAll){
			return String.valueOf(countOfAll);
		}
		return String.valueOf(countOfVieved);
	}
	
	public void resetPageble() {
		pageable = new PageRequest(0, PAGE_SIZE, Sort.Direction.DESC, "id");
	}
	
	public String getPreviousButtonClasses() {
		Long countOfAll = getCountOfAllBooks();
		if(countOfAll <= PAGE_SIZE) {
			return "btn disabled";
		}
		return pageable.getOffset() == 0 ? "btn disabled" : "";
	}
	
	public String getNextButtonClasses() {
		Long countOfAll = getCountOfAllBooks();
		if(countOfAll <= PAGE_SIZE) {
			return "btn disabled";
		}
		return pageable.getOffset() + pageable.getPageSize() >= getCountOfAllBooks() ? "btn disabled" : "";
	}
	
/*	public void setStatusType(AtmStatusType atmStatusType) {
		this.atmStatusType = atmStatusType;
	}
	
	public AtmStatusType getStatusType() {
		return atmStatusType;
	}*/
	
/*	public String getDangerButtonClasses() {
		return atmStatusType != null && atmStatusType.value().equals(AtmStatusType.ERROR.value()) ? "clicked" : "";
	}
	
	public String getWarningButtonClasses() {
		return atmStatusType != null && atmStatusType.value().equals(AtmStatusType.WARNING.value()) ? "clicked" : "";
	}
	
	public String getPowerOffButtonClasses() {
		return atmStatusType != null && atmStatusType.value().equals(AtmStatusType.POWEROFF.value()) ? "clicked" : "";
	}*/
	

	
/*	public String getPanelStatusClasses(AtmStatusType atmStatusType) {
		if(atmStatusType != null && atmStatusType.value().equals(AtmStatusType.ERROR.value())){
			return "panel-danger";
		}
		else if(atmStatusType != null && atmStatusType.value().equals(AtmStatusType.WARNING.value())){
			return "panel-warning";
		}
		else if(atmStatusType != null && atmStatusType.value().equals(AtmStatusType.OK.value())){
			return "panel-success";
		}
		else if(atmStatusType != null && atmStatusType.value().equals(AtmStatusType.POWEROFF.value())){
			return "panel-poweroff";
		}
		else {
			return "panel-default";
		}
	}*/
	
/*	public String getPanelStatusName(AtmStatusType atmStatusType) {
		if(atmStatusType != null && atmStatusType.value().equals(AtmStatusType.ERROR.value())){
			return "ERROR";
		}
		else if(atmStatusType != null && atmStatusType.value().equals(AtmStatusType.WARNING.value())){
			return "WARNING";
		}
		else if(atmStatusType != null && atmStatusType.value().equals(AtmStatusType.OK.value())){
			return "OK";
		}
		else if(atmStatusType != null && atmStatusType.value().equals(AtmStatusType.POWEROFF.value())){
			return "POWEROFF";
		}
		else {
			return "UNDEFINED";
		}
	}*/
	
/*	public String getCommonTdStatusClasses(CommonStatusType printerStatusType) {
		if(printerStatusType != null && printerStatusType.value().equals(CommonStatusType.ERROR.value())){
			return "error";
		}
		else if(printerStatusType != null && printerStatusType.value().equals(CommonStatusType.WARNING.value())){
			return "warning";
		}
		else if(printerStatusType != null && printerStatusType.value().equals(CommonStatusType.OK.value())){
			return "ok";
		}
		else {
			return "error";
		}
	}*/
}
