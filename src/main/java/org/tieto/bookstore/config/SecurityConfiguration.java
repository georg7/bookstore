package org.tieto.bookstore.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.access.AccessDeniedHandler;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
/*	@Autowired
	private UserDetailsService userDetailsService;*/
	
	@Autowired
    private AccessDeniedHandler accessDeniedHandler;

	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {
		/*
		 * httpSecurity.authorizeRequests().antMatchers("/").permitAll().and()
		 * .authorizeRequests().antMatchers("/console/**").permitAll();
		 */

		// httpSecurity.csrf().disable();
		// httpSecurity.headers().frameOptions().disable();

/*		httpSecurity.authorizeRequests().antMatchers("/webjars/**", "/images/**", "/js/**", "/css/**").permitAll()
				.anyRequest().authenticated().and().formLogin().loginPage("/login").permitAll().and().logout()
				.permitAll();*/
		
		httpSecurity.
		authorizeRequests()
			.antMatchers("/", "/logout").permitAll()
			.antMatchers("/administration/**").hasRole("ADMIN")
			.antMatchers("/settings/**").hasRole("ADMIN")
			.antMatchers("/atms/**").hasRole("ADMIN")
			.antMatchers("/serversettings/**").hasRole("ADMIN")
			.antMatchers("/atmsettings/**").hasRole("ADMIN")
			.antMatchers("/developer/**").hasRole("ACTUATOR")
			.anyRequest()
			.authenticated().and().formLogin()
			.loginPage("/login")
			.usernameParameter("email")
			.passwordParameter("password")
			.permitAll()
			.and().csrf().ignoringAntMatchers("/logout")
			
			.and().logout()
			.permitAll()
			.and()
            .exceptionHandling().accessDeniedHandler(accessDeniedHandler);

	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/webjars/**", "/css/**", "/js/**", "/images/**", "/ws/**");
	}

}