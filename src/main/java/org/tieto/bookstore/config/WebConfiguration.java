package org.tieto.bookstore.config;

import org.tieto.bookstore.interceptors.BreadCrumbInterceptor;
import org.tieto.bookstore.interceptors.TopMenuInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

//@EnableWebMvc
@Configuration
public class WebConfiguration extends WebMvcConfigurerAdapter {
	@Autowired
	BreadCrumbInterceptor breadCrumbInterceptor;
	
	@Autowired
	TopMenuInterceptor topMenuInterceptor;
	
	@Bean
	BreadCrumbInterceptor breadCrumbInterceptor() {
         return new BreadCrumbInterceptor();
    }
	
	@Bean
	TopMenuInterceptor topMenuInterceptor() {
         return new TopMenuInterceptor();
    }
	
	@Override
	  public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(breadCrumbInterceptor());
		registry.addInterceptor(topMenuInterceptor());
	}
	
	@Bean(name = "messageSource")
	public MessageSource messageSource() {
	    ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
	    messageSource.setBasename("validation");
	    messageSource.setDefaultEncoding("UTF-8");
	    return messageSource;
	}
	
	
}
