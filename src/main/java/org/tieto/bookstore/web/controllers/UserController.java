package org.tieto.bookstore.web.controllers;

import java.util.Collections;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.tieto.bookstore.annotations.Link;
import org.tieto.bookstore.services.ITopMenuService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@ControllerAdvice
public class UserController {
	
	private static Logger logger = LoggerFactory.getLogger(UserController.class);
	
	private ITopMenuService topMenuService;
	
    @Autowired
    public void setTopMenuService(ITopMenuService topMenuService) {
        this.topMenuService = topMenuService;
    }
    
    @ModelAttribute
    public void addAttributes(Model model, HttpServletRequest request) {
    	model.addAttribute("topmenus", topMenuService.listAllTopMenus());
    }
    
    @ModelAttribute("currentUser")
    public UserDetails getCurrentUser(Authentication authentication) {
        return (authentication == null) ? null : (UserDetails) authentication.getPrincipal();
    }
    
    @ModelAttribute("sessionAttrNames")
    public List<String> getSessionAttrNames(HttpSession session) {
        return Collections.list(session.getAttributeNames());
    }
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	String login(Model model, String error, String logout) {
        if (error != null)
            model.addAttribute("error", "Your username or password is invalid.");

        if (logout != null)
            model.addAttribute("message", "You have been logged out successfully.");
		return "login";
	}
	
	@Link(label="Welcome", family="WelcomeController", parent = "" )
    @RequestMapping(value = {"/"}, method = RequestMethod.GET)
    public String welcome(Model model, HttpSession session) {
        return "redirect:/books";
    }
	
	@RequestMapping("/403")
	public String accessDenied() {
	    return "errors/403";
	}
	
}
