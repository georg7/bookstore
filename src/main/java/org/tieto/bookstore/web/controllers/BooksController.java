package org.tieto.bookstore.web.controllers;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.tieto.bookstore.annotations.Link;
import org.tieto.bookstore.db.entities.Book;
import org.tieto.bookstore.services.IBookService;
import org.tieto.bookstore.utils.BooksPage;
import org.tieto.bookstore.validators.BookValidator;

import com.sun.jersey.api.client.ClientHandlerException;

@Controller
@SessionAttributes(value = {"booksPage","bookForm"})
public class BooksController {

	@Autowired
	private IBookService bookService;

	@Autowired
	private BooksPage booksPage;

	@Autowired
	private BookValidator bookValidator;
	
/*	@Autowired
	private IPayOutDataWebService payOutDataWebService;*/

	@Link(label = "Books", family = "BooksController", parent = "Books")
	@RequestMapping(value = "/books", method = RequestMethod.GET)
	public String overview(Model model, @RequestParam(value = "direction", required = false) String direction,
			@RequestParam(value = "filter", required = false) String filter) {

		model.addAttribute("booksPage", booksPage);
		
		/* Direction */
		if (direction != null && direction.equals("next")) {
			booksPage.nextPage();
		} else if (direction != null && direction.equals("previous")) {
			booksPage.previousPage();
		}

		/* Button filter */
		if(direction == null) {
			booksPage.resetPageble();
		}
		if (filter != null && filter.equals("error")) {
			//overviewPage.setStatusType(AtmStatusType.ERROR);
		} else if (filter != null && filter.equals("warning")) {
			//overviewPage.setStatusType(AtmStatusType.WARNING);
		} else if (filter != null && filter.equals("poweroff")) {
			//overviewPage.setStatusType(AtmStatusType.POWEROFF);
		} else if (filter != null && filter.equals("clear")) {
			//overviewPage.setStatusType(null);
		}
		return "books";
	}


/*	@Link(label = "Add Book", family = "BooksController", parent = "Books")
	@RequestMapping(value = "/atms", method = RequestMethod.GET)
	public String atms(Model model) {
		List<Book> atms = atmService.findAtmAll();
		model.addAttribute("atms", atms);
		return "atms";
	}*/

	@Link(label = "Add Book", family = "BooksController", parent = "Books")
	@RequestMapping(value = "/book/add", method = RequestMethod.GET)
	public String atmsadd(Model model) {
		Book book = new Book();
		model.addAttribute("bookForm", book);
		return "addbook";
	}
	
	
	
	@Link(label = "Add Book", family = "BooksController", parent = "Books")
	@RequestMapping(value = "/book/add", method = RequestMethod.POST)
	public String atmsadd(@ModelAttribute("bookForm") Book book, BindingResult bindingResult, Model model) {
		bookValidator.validate(book, bindingResult);
		if (bindingResult.hasErrors()) {
			return "addbook";
		}
		bookService.save(book);
		return "redirect:/books";
	}
	
	/*
	
	@RequestMapping(value = "/atm/{id}/delete", method = RequestMethod.GET)
	public String userdelete(@PathVariable Long id, Model model) {
		atmService.delete(id);
		return "redirect:/atms";
	}
	
	@Link(label = "Edit Atm", family = "WelcomeController", parent = "Atms")
	@RequestMapping(value = "/atm/{id}/edit", method = RequestMethod.GET)
	public String atmedit(@PathVariable Long id, Model model) {
		Book atm = atmService.findOne(id);
		model.addAttribute("atmForm", atm);
		model.addAttribute("editButtonTitle", "Edit Atm");
		return "addatm";
	}
	
	@Link(label = "Printer details", family = "WelcomeController", parent = "Welcome")
	@RequestMapping(value = "/atm/{id}/printerdetails", method = RequestMethod.GET)
	public String printerdetails(@PathVariable Long id, Model model) {
		printerDetailsPage.setPrinter(id);
		model.addAttribute("printerDetailsPage", printerDetailsPage);
		
		return "printerdetails";
	}
	
	@Link(label = "Recycler details", family = "WelcomeController", parent = "Welcome")
	@RequestMapping(value = "/atm/{id}/recyclerdetails", method = RequestMethod.GET)
	public String recyclerdetails(@PathVariable Long id, Model model) {
		recyclerDetailsPage.setRecycler(id);
		model.addAttribute("recyclerDetailsPage", recyclerDetailsPage);
		
		return "recyclerdetails";
	}
	
	
	
	*//**
	 * Ajax
	 *//*
	@RequestMapping(value = "/atm/{atmId}/dosystemaction", method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody String dosystemaction(
			@PathVariable Long atmId, 
			@RequestParam String action, 
			HttpServletResponse response) {
		
		Book atm = atmService.findOne(atmId);
		String responseStr = null;
		
		try {
			if (atm != null) {
				if (action.equals("restart")) {
					responseStr = payOutDataWebService.doSystemRestart(atm);
				} else if(action.equals("shutdown")) {
					responseStr = payOutDataWebService.doSystemShutdown(atm);
				}
				else {
					response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				}
				
				if(responseStr != null) {
					System.out.println("******************* Response from ATM: " + responseStr + " ATM: " + atm.getAtmIdentifier());
				}
				
			} else {
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			}
			
		}catch(ClientHandlerException e) {
			System.out.println("*************************** RESTART OR SHUTDOWN *************************");
			System.out.println(responseStr);
			e.printStackTrace();
		}



		return responseStr;
	}*/

}
