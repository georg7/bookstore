package org.tieto.bookstore.validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.tieto.bookstore.db.entities.Book;
import org.tieto.bookstore.db.entities.User;
import org.tieto.bookstore.services.IBookService;
import org.tieto.bookstore.services.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class BookValidator implements Validator {
	
	private Pattern pattern;
	private Matcher matcher;
	
	private static final String BOOK_ISBN_PATTERN = "^[0-9]{1}-[0-9]{3}-[0-9]{5}-[0-9]{1}$";
	
	@Autowired
	private IBookService bookService;

	@Override
	public boolean supports(Class<?> clazz) {
		return User.class.equals(clazz);
	}

	@Override
	public void validate(Object o, Errors errors) {
		Book book = (Book) o;
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "bookName", "bookForm.bookName.empty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "bookISBN", "bookForm.bookISBN.empty");

		// Book ISBN format validation 
		if (!(book.getBookISBN() != null && book.getBookISBN().isEmpty())) {
			pattern = Pattern.compile(BOOK_ISBN_PATTERN);
			matcher = pattern.matcher(book.getBookISBN());
			if (!matcher.matches()) {
				errors.rejectValue("bookISBN", "bookForm.bookISBN.format.incorrect");
			}
		}
		
		// Book name uniquiness
		if (book.getId() == null && !bookService.findAllByName(book.getBookName()).isEmpty()) {
			errors.rejectValue("bookName", "bookForm.bookName.already.in.use");
		}
		
	}

}
