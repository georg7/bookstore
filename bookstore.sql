-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 30, 2018 at 12:30 PM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bookstore`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`bookstore`@`%` PROCEDURE `myproc` ()  BEGIN
    DECLARE i int DEFAULT 500;
    WHILE i <= 500 AND i >= 0 DO
        INSERT INTO books (book_isbn, book_name, book_price) VALUES ('0-596-52068-9', CONCAT(i, 'book'), null);
        SET i = i - 1;
    END WHILE;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `id` bigint(20) NOT NULL,
  `book_isbn` varchar(255) NOT NULL,
  `book_name` varchar(255) NOT NULL,
  `book_price` varchar(255) DEFAULT NULL,
  `search_string` varchar(1001) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `book_isbn`, `book_name`, `book_price`, `search_string`) VALUES
(502, '0-596-52068-9', '500book', NULL, NULL),
(503, '0-596-52068-9', '499book', NULL, NULL),
(504, '0-596-52068-9', '498book', NULL, NULL),
(505, '0-596-52068-9', '497book', NULL, NULL),
(506, '0-596-52068-9', '496book', NULL, NULL),
(507, '0-596-52068-9', '495book', NULL, NULL),
(508, '0-596-52068-9', '494book', NULL, NULL),
(509, '0-596-52068-9', '493book', NULL, NULL),
(510, '0-596-52068-9', '492book', NULL, NULL),
(511, '0-596-52068-9', '491book', NULL, NULL),
(512, '0-596-52068-9', '490book', NULL, NULL),
(513, '0-596-52068-9', '489book', NULL, NULL),
(514, '0-596-52068-9', '488book', NULL, NULL),
(515, '0-596-52068-9', '487book', NULL, NULL),
(516, '0-596-52068-9', '486book', NULL, NULL),
(517, '0-596-52068-9', '485book', NULL, NULL),
(518, '0-596-52068-9', '484book', NULL, NULL),
(519, '0-596-52068-9', '483book', NULL, NULL),
(520, '0-596-52068-9', '482book', NULL, NULL),
(521, '0-596-52068-9', '481book', NULL, NULL),
(522, '0-596-52068-9', '480book', NULL, NULL),
(523, '0-596-52068-9', '479book', NULL, NULL),
(524, '0-596-52068-9', '478book', NULL, NULL),
(525, '0-596-52068-9', '477book', NULL, NULL),
(526, '0-596-52068-9', '476book', NULL, NULL),
(527, '0-596-52068-9', '475book', NULL, NULL),
(528, '0-596-52068-9', '474book', NULL, NULL),
(529, '0-596-52068-9', '473book', NULL, NULL),
(530, '0-596-52068-9', '472book', NULL, NULL),
(531, '0-596-52068-9', '471book', NULL, NULL),
(532, '0-596-52068-9', '470book', NULL, NULL),
(533, '0-596-52068-9', '469book', NULL, NULL),
(534, '0-596-52068-9', '468book', NULL, NULL),
(535, '0-596-52068-9', '467book', NULL, NULL),
(536, '0-596-52068-9', '466book', NULL, NULL),
(537, '0-596-52068-9', '465book', NULL, NULL),
(538, '0-596-52068-9', '464book', NULL, NULL),
(539, '0-596-52068-9', '463book', NULL, NULL),
(540, '0-596-52068-9', '462book', NULL, NULL),
(541, '0-596-52068-9', '461book', NULL, NULL),
(542, '0-596-52068-9', '460book', NULL, NULL),
(543, '0-596-52068-9', '459book', NULL, NULL),
(544, '0-596-52068-9', '458book', NULL, NULL),
(545, '0-596-52068-9', '457book', NULL, NULL),
(546, '0-596-52068-9', '456book', NULL, NULL),
(547, '0-596-52068-9', '455book', NULL, NULL),
(548, '0-596-52068-9', '454book', NULL, NULL),
(549, '0-596-52068-9', '453book', NULL, NULL),
(550, '0-596-52068-9', '452book', NULL, NULL),
(551, '0-596-52068-9', '451book', NULL, NULL),
(552, '0-596-52068-9', '450book', NULL, NULL),
(553, '0-596-52068-9', '449book', NULL, NULL),
(554, '0-596-52068-9', '448book', NULL, NULL),
(555, '0-596-52068-9', '447book', NULL, NULL),
(556, '0-596-52068-9', '446book', NULL, NULL),
(557, '0-596-52068-9', '445book', NULL, NULL),
(558, '0-596-52068-9', '444book', NULL, NULL),
(559, '0-596-52068-9', '443book', NULL, NULL),
(560, '0-596-52068-9', '442book', NULL, NULL),
(561, '0-596-52068-9', '441book', NULL, NULL),
(562, '0-596-52068-9', '440book', NULL, NULL),
(563, '0-596-52068-9', '439book', NULL, NULL),
(564, '0-596-52068-9', '438book', NULL, NULL),
(565, '0-596-52068-9', '437book', NULL, NULL),
(566, '0-596-52068-9', '436book', NULL, NULL),
(567, '0-596-52068-9', '435book', NULL, NULL),
(568, '0-596-52068-9', '434book', NULL, NULL),
(569, '0-596-52068-9', '433book', NULL, NULL),
(570, '0-596-52068-9', '432book', NULL, NULL),
(571, '0-596-52068-9', '431book', NULL, NULL),
(572, '0-596-52068-9', '430book', NULL, NULL),
(573, '0-596-52068-9', '429book', NULL, NULL),
(574, '0-596-52068-9', '428book', NULL, NULL),
(575, '0-596-52068-9', '427book', NULL, NULL),
(576, '0-596-52068-9', '426book', NULL, NULL),
(577, '0-596-52068-9', '425book', NULL, NULL),
(578, '0-596-52068-9', '424book', NULL, NULL),
(579, '0-596-52068-9', '423book', NULL, NULL),
(580, '0-596-52068-9', '422book', NULL, NULL),
(581, '0-596-52068-9', '421book', NULL, NULL),
(582, '0-596-52068-9', '420book', NULL, NULL),
(583, '0-596-52068-9', '419book', NULL, NULL),
(584, '0-596-52068-9', '418book', NULL, NULL),
(585, '0-596-52068-9', '417book', NULL, NULL),
(586, '0-596-52068-9', '416book', NULL, NULL),
(587, '0-596-52068-9', '415book', NULL, NULL),
(588, '0-596-52068-9', '414book', NULL, NULL),
(589, '0-596-52068-9', '413book', NULL, NULL),
(590, '0-596-52068-9', '412book', NULL, NULL),
(591, '0-596-52068-9', '411book', NULL, NULL),
(592, '0-596-52068-9', '410book', NULL, NULL),
(593, '0-596-52068-9', '409book', NULL, NULL),
(594, '0-596-52068-9', '408book', NULL, NULL),
(595, '0-596-52068-9', '407book', NULL, NULL),
(596, '0-596-52068-9', '406book', NULL, NULL),
(597, '0-596-52068-9', '405book', NULL, NULL),
(598, '0-596-52068-9', '404book', NULL, NULL),
(599, '0-596-52068-9', '403book', NULL, NULL),
(600, '0-596-52068-9', '402book', NULL, NULL),
(601, '0-596-52068-9', '401book', NULL, NULL),
(602, '0-596-52068-9', '400book', NULL, NULL),
(603, '0-596-52068-9', '399book', NULL, NULL),
(604, '0-596-52068-9', '398book', NULL, NULL),
(605, '0-596-52068-9', '397book', NULL, NULL),
(606, '0-596-52068-9', '396book', NULL, NULL),
(607, '0-596-52068-9', '395book', NULL, NULL),
(608, '0-596-52068-9', '394book', NULL, NULL),
(609, '0-596-52068-9', '393book', NULL, NULL),
(610, '0-596-52068-9', '392book', NULL, NULL),
(611, '0-596-52068-9', '391book', NULL, NULL),
(612, '0-596-52068-9', '390book', NULL, NULL),
(613, '0-596-52068-9', '389book', NULL, NULL),
(614, '0-596-52068-9', '388book', NULL, NULL),
(615, '0-596-52068-9', '387book', NULL, NULL),
(616, '0-596-52068-9', '386book', NULL, NULL),
(617, '0-596-52068-9', '385book', NULL, NULL),
(618, '0-596-52068-9', '384book', NULL, NULL),
(619, '0-596-52068-9', '383book', NULL, NULL),
(620, '0-596-52068-9', '382book', NULL, NULL),
(621, '0-596-52068-9', '381book', NULL, NULL),
(622, '0-596-52068-9', '380book', NULL, NULL),
(623, '0-596-52068-9', '379book', NULL, NULL),
(624, '0-596-52068-9', '378book', NULL, NULL),
(625, '0-596-52068-9', '377book', NULL, NULL),
(626, '0-596-52068-9', '376book', NULL, NULL),
(627, '0-596-52068-9', '375book', NULL, NULL),
(628, '0-596-52068-9', '374book', NULL, NULL),
(629, '0-596-52068-9', '373book', NULL, NULL),
(630, '0-596-52068-9', '372book', NULL, NULL),
(631, '0-596-52068-9', '371book', NULL, NULL),
(632, '0-596-52068-9', '370book', NULL, NULL),
(633, '0-596-52068-9', '369book', NULL, NULL),
(634, '0-596-52068-9', '368book', NULL, NULL),
(635, '0-596-52068-9', '367book', NULL, NULL),
(636, '0-596-52068-9', '366book', NULL, NULL),
(637, '0-596-52068-9', '365book', NULL, NULL),
(638, '0-596-52068-9', '364book', NULL, NULL),
(639, '0-596-52068-9', '363book', NULL, NULL),
(640, '0-596-52068-9', '362book', NULL, NULL),
(641, '0-596-52068-9', '361book', NULL, NULL),
(642, '0-596-52068-9', '360book', NULL, NULL),
(643, '0-596-52068-9', '359book', NULL, NULL),
(644, '0-596-52068-9', '358book', NULL, NULL),
(645, '0-596-52068-9', '357book', NULL, NULL),
(646, '0-596-52068-9', '356book', NULL, NULL),
(647, '0-596-52068-9', '355book', NULL, NULL),
(648, '0-596-52068-9', '354book', NULL, NULL),
(649, '0-596-52068-9', '353book', NULL, NULL),
(650, '0-596-52068-9', '352book', NULL, NULL),
(651, '0-596-52068-9', '351book', NULL, NULL),
(652, '0-596-52068-9', '350book', NULL, NULL),
(653, '0-596-52068-9', '349book', NULL, NULL),
(654, '0-596-52068-9', '348book', NULL, NULL),
(655, '0-596-52068-9', '347book', NULL, NULL),
(656, '0-596-52068-9', '346book', NULL, NULL),
(657, '0-596-52068-9', '345book', NULL, NULL),
(658, '0-596-52068-9', '344book', NULL, NULL),
(659, '0-596-52068-9', '343book', NULL, NULL),
(660, '0-596-52068-9', '342book', NULL, NULL),
(661, '0-596-52068-9', '341book', NULL, NULL),
(662, '0-596-52068-9', '340book', NULL, NULL),
(663, '0-596-52068-9', '339book', NULL, NULL),
(664, '0-596-52068-9', '338book', NULL, NULL),
(665, '0-596-52068-9', '337book', NULL, NULL),
(666, '0-596-52068-9', '336book', NULL, NULL),
(667, '0-596-52068-9', '335book', NULL, NULL),
(668, '0-596-52068-9', '334book', NULL, NULL),
(669, '0-596-52068-9', '333book', NULL, NULL),
(670, '0-596-52068-9', '332book', NULL, NULL),
(671, '0-596-52068-9', '331book', NULL, NULL),
(672, '0-596-52068-9', '330book', NULL, NULL),
(673, '0-596-52068-9', '329book', NULL, NULL),
(674, '0-596-52068-9', '328book', NULL, NULL),
(675, '0-596-52068-9', '327book', NULL, NULL),
(676, '0-596-52068-9', '326book', NULL, NULL),
(677, '0-596-52068-9', '325book', NULL, NULL),
(678, '0-596-52068-9', '324book', NULL, NULL),
(679, '0-596-52068-9', '323book', NULL, NULL),
(680, '0-596-52068-9', '322book', NULL, NULL),
(681, '0-596-52068-9', '321book', NULL, NULL),
(682, '0-596-52068-9', '320book', NULL, NULL),
(683, '0-596-52068-9', '319book', NULL, NULL),
(684, '0-596-52068-9', '318book', NULL, NULL),
(685, '0-596-52068-9', '317book', NULL, NULL),
(686, '0-596-52068-9', '316book', NULL, NULL),
(687, '0-596-52068-9', '315book', NULL, NULL),
(688, '0-596-52068-9', '314book', NULL, NULL),
(689, '0-596-52068-9', '313book', NULL, NULL),
(690, '0-596-52068-9', '312book', NULL, NULL),
(691, '0-596-52068-9', '311book', NULL, NULL),
(692, '0-596-52068-9', '310book', NULL, NULL),
(693, '0-596-52068-9', '309book', NULL, NULL),
(694, '0-596-52068-9', '308book', NULL, NULL),
(695, '0-596-52068-9', '307book', NULL, NULL),
(696, '0-596-52068-9', '306book', NULL, NULL),
(697, '0-596-52068-9', '305book', NULL, NULL),
(698, '0-596-52068-9', '304book', NULL, NULL),
(699, '0-596-52068-9', '303book', NULL, NULL),
(700, '0-596-52068-9', '302book', NULL, NULL),
(701, '0-596-52068-9', '301book', NULL, NULL),
(702, '0-596-52068-9', '300book', NULL, NULL),
(703, '0-596-52068-9', '299book', NULL, NULL),
(704, '0-596-52068-9', '298book', NULL, NULL),
(705, '0-596-52068-9', '297book', NULL, NULL),
(706, '0-596-52068-9', '296book', NULL, NULL),
(707, '0-596-52068-9', '295book', NULL, NULL),
(708, '0-596-52068-9', '294book', NULL, NULL),
(709, '0-596-52068-9', '293book', NULL, NULL),
(710, '0-596-52068-9', '292book', NULL, NULL),
(711, '0-596-52068-9', '291book', NULL, NULL),
(712, '0-596-52068-9', '290book', NULL, NULL),
(713, '0-596-52068-9', '289book', NULL, NULL),
(714, '0-596-52068-9', '288book', NULL, NULL),
(715, '0-596-52068-9', '287book', NULL, NULL),
(716, '0-596-52068-9', '286book', NULL, NULL),
(717, '0-596-52068-9', '285book', NULL, NULL),
(718, '0-596-52068-9', '284book', NULL, NULL),
(719, '0-596-52068-9', '283book', NULL, NULL),
(720, '0-596-52068-9', '282book', NULL, NULL),
(721, '0-596-52068-9', '281book', NULL, NULL),
(722, '0-596-52068-9', '280book', NULL, NULL),
(723, '0-596-52068-9', '279book', NULL, NULL),
(724, '0-596-52068-9', '278book', NULL, NULL),
(725, '0-596-52068-9', '277book', NULL, NULL),
(726, '0-596-52068-9', '276book', NULL, NULL),
(727, '0-596-52068-9', '275book', NULL, NULL),
(728, '0-596-52068-9', '274book', NULL, NULL),
(729, '0-596-52068-9', '273book', NULL, NULL),
(730, '0-596-52068-9', '272book', NULL, NULL),
(731, '0-596-52068-9', '271book', NULL, NULL),
(732, '0-596-52068-9', '270book', NULL, NULL),
(733, '0-596-52068-9', '269book', NULL, NULL),
(734, '0-596-52068-9', '268book', NULL, NULL),
(735, '0-596-52068-9', '267book', NULL, NULL),
(736, '0-596-52068-9', '266book', NULL, NULL),
(737, '0-596-52068-9', '265book', NULL, NULL),
(738, '0-596-52068-9', '264book', NULL, NULL),
(739, '0-596-52068-9', '263book', NULL, NULL),
(740, '0-596-52068-9', '262book', NULL, NULL),
(741, '0-596-52068-9', '261book', NULL, NULL),
(742, '0-596-52068-9', '260book', NULL, NULL),
(743, '0-596-52068-9', '259book', NULL, NULL),
(744, '0-596-52068-9', '258book', NULL, NULL),
(745, '0-596-52068-9', '257book', NULL, NULL),
(746, '0-596-52068-9', '256book', NULL, NULL),
(747, '0-596-52068-9', '255book', NULL, NULL),
(748, '0-596-52068-9', '254book', NULL, NULL),
(749, '0-596-52068-9', '253book', NULL, NULL),
(750, '0-596-52068-9', '252book', NULL, NULL),
(751, '0-596-52068-9', '251book', NULL, NULL),
(752, '0-596-52068-9', '250book', NULL, NULL),
(753, '0-596-52068-9', '249book', NULL, NULL),
(754, '0-596-52068-9', '248book', NULL, NULL),
(755, '0-596-52068-9', '247book', NULL, NULL),
(756, '0-596-52068-9', '246book', NULL, NULL),
(757, '0-596-52068-9', '245book', NULL, NULL),
(758, '0-596-52068-9', '244book', NULL, NULL),
(759, '0-596-52068-9', '243book', NULL, NULL),
(760, '0-596-52068-9', '242book', NULL, NULL),
(761, '0-596-52068-9', '241book', NULL, NULL),
(762, '0-596-52068-9', '240book', NULL, NULL),
(763, '0-596-52068-9', '239book', NULL, NULL),
(764, '0-596-52068-9', '238book', NULL, NULL),
(765, '0-596-52068-9', '237book', NULL, NULL),
(766, '0-596-52068-9', '236book', NULL, NULL),
(767, '0-596-52068-9', '235book', NULL, NULL),
(768, '0-596-52068-9', '234book', NULL, NULL),
(769, '0-596-52068-9', '233book', NULL, NULL),
(770, '0-596-52068-9', '232book', NULL, NULL),
(771, '0-596-52068-9', '231book', NULL, NULL),
(772, '0-596-52068-9', '230book', NULL, NULL),
(773, '0-596-52068-9', '229book', NULL, NULL),
(774, '0-596-52068-9', '228book', NULL, NULL),
(775, '0-596-52068-9', '227book', NULL, NULL),
(776, '0-596-52068-9', '226book', NULL, NULL),
(777, '0-596-52068-9', '225book', NULL, NULL),
(778, '0-596-52068-9', '224book', NULL, NULL),
(779, '0-596-52068-9', '223book', NULL, NULL),
(780, '0-596-52068-9', '222book', NULL, NULL),
(781, '0-596-52068-9', '221book', NULL, NULL),
(782, '0-596-52068-9', '220book', NULL, NULL),
(783, '0-596-52068-9', '219book', NULL, NULL),
(784, '0-596-52068-9', '218book', NULL, NULL),
(785, '0-596-52068-9', '217book', NULL, NULL),
(786, '0-596-52068-9', '216book', NULL, NULL),
(787, '0-596-52068-9', '215book', NULL, NULL),
(788, '0-596-52068-9', '214book', NULL, NULL),
(789, '0-596-52068-9', '213book', NULL, NULL),
(790, '0-596-52068-9', '212book', NULL, NULL),
(791, '0-596-52068-9', '211book', NULL, NULL),
(792, '0-596-52068-9', '210book', NULL, NULL),
(793, '0-596-52068-9', '209book', NULL, NULL),
(794, '0-596-52068-9', '208book', NULL, NULL),
(795, '0-596-52068-9', '207book', NULL, NULL),
(796, '0-596-52068-9', '206book', NULL, NULL),
(797, '0-596-52068-9', '205book', NULL, NULL),
(798, '0-596-52068-9', '204book', NULL, NULL),
(799, '0-596-52068-9', '203book', NULL, NULL),
(800, '0-596-52068-9', '202book', NULL, NULL),
(801, '0-596-52068-9', '201book', NULL, NULL),
(802, '0-596-52068-9', '200book', NULL, NULL),
(803, '0-596-52068-9', '199book', NULL, NULL),
(804, '0-596-52068-9', '198book', NULL, NULL),
(805, '0-596-52068-9', '197book', NULL, NULL),
(806, '0-596-52068-9', '196book', NULL, NULL),
(807, '0-596-52068-9', '195book', NULL, NULL),
(808, '0-596-52068-9', '194book', NULL, NULL),
(809, '0-596-52068-9', '193book', NULL, NULL),
(810, '0-596-52068-9', '192book', NULL, NULL),
(811, '0-596-52068-9', '191book', NULL, NULL),
(812, '0-596-52068-9', '190book', NULL, NULL),
(813, '0-596-52068-9', '189book', NULL, NULL),
(814, '0-596-52068-9', '188book', NULL, NULL),
(815, '0-596-52068-9', '187book', NULL, NULL),
(816, '0-596-52068-9', '186book', NULL, NULL),
(817, '0-596-52068-9', '185book', NULL, NULL),
(818, '0-596-52068-9', '184book', NULL, NULL),
(819, '0-596-52068-9', '183book', NULL, NULL),
(820, '0-596-52068-9', '182book', NULL, NULL),
(821, '0-596-52068-9', '181book', NULL, NULL),
(822, '0-596-52068-9', '180book', NULL, NULL),
(823, '0-596-52068-9', '179book', NULL, NULL),
(824, '0-596-52068-9', '178book', NULL, NULL),
(825, '0-596-52068-9', '177book', NULL, NULL),
(826, '0-596-52068-9', '176book', NULL, NULL),
(827, '0-596-52068-9', '175book', NULL, NULL),
(828, '0-596-52068-9', '174book', NULL, NULL),
(829, '0-596-52068-9', '173book', NULL, NULL),
(830, '0-596-52068-9', '172book', NULL, NULL),
(831, '0-596-52068-9', '171book', NULL, NULL),
(832, '0-596-52068-9', '170book', NULL, NULL),
(833, '0-596-52068-9', '169book', NULL, NULL),
(834, '0-596-52068-9', '168book', NULL, NULL),
(835, '0-596-52068-9', '167book', NULL, NULL),
(836, '0-596-52068-9', '166book', NULL, NULL),
(837, '0-596-52068-9', '165book', NULL, NULL),
(838, '0-596-52068-9', '164book', NULL, NULL),
(839, '0-596-52068-9', '163book', NULL, NULL),
(840, '0-596-52068-9', '162book', NULL, NULL),
(841, '0-596-52068-9', '161book', NULL, NULL),
(842, '0-596-52068-9', '160book', NULL, NULL),
(843, '0-596-52068-9', '159book', NULL, NULL),
(844, '0-596-52068-9', '158book', NULL, NULL),
(845, '0-596-52068-9', '157book', NULL, NULL),
(846, '0-596-52068-9', '156book', NULL, NULL),
(847, '0-596-52068-9', '155book', NULL, NULL),
(848, '0-596-52068-9', '154book', NULL, NULL),
(849, '0-596-52068-9', '153book', NULL, NULL),
(850, '0-596-52068-9', '152book', NULL, NULL),
(851, '0-596-52068-9', '151book', NULL, NULL),
(852, '0-596-52068-9', '150book', NULL, NULL),
(853, '0-596-52068-9', '149book', NULL, NULL),
(854, '0-596-52068-9', '148book', NULL, NULL),
(855, '0-596-52068-9', '147book', NULL, NULL),
(856, '0-596-52068-9', '146book', NULL, NULL),
(857, '0-596-52068-9', '145book', NULL, NULL),
(858, '0-596-52068-9', '144book', NULL, NULL),
(859, '0-596-52068-9', '143book', NULL, NULL),
(860, '0-596-52068-9', '142book', NULL, NULL),
(861, '0-596-52068-9', '141book', NULL, NULL),
(862, '0-596-52068-9', '140book', NULL, NULL),
(863, '0-596-52068-9', '139book', NULL, NULL),
(864, '0-596-52068-9', '138book', NULL, NULL),
(865, '0-596-52068-9', '137book', NULL, NULL),
(866, '0-596-52068-9', '136book', NULL, NULL),
(867, '0-596-52068-9', '135book', NULL, NULL),
(868, '0-596-52068-9', '134book', NULL, NULL),
(869, '0-596-52068-9', '133book', NULL, NULL),
(870, '0-596-52068-9', '132book', NULL, NULL),
(871, '0-596-52068-9', '131book', NULL, NULL),
(872, '0-596-52068-9', '130book', NULL, NULL),
(873, '0-596-52068-9', '129book', NULL, NULL),
(874, '0-596-52068-9', '128book', NULL, NULL),
(875, '0-596-52068-9', '127book', NULL, NULL),
(876, '0-596-52068-9', '126book', NULL, NULL),
(877, '0-596-52068-9', '125book', NULL, NULL),
(878, '0-596-52068-9', '124book', NULL, NULL),
(879, '0-596-52068-9', '123book', NULL, NULL),
(880, '0-596-52068-9', '122book', NULL, NULL),
(881, '0-596-52068-9', '121book', NULL, NULL),
(882, '0-596-52068-9', '120book', NULL, NULL),
(883, '0-596-52068-9', '119book', NULL, NULL),
(884, '0-596-52068-9', '118book', NULL, NULL),
(885, '0-596-52068-9', '117book', NULL, NULL),
(886, '0-596-52068-9', '116book', NULL, NULL),
(887, '0-596-52068-9', '115book', NULL, NULL),
(888, '0-596-52068-9', '114book', NULL, NULL),
(889, '0-596-52068-9', '113book', NULL, NULL),
(890, '0-596-52068-9', '112book', NULL, NULL),
(891, '0-596-52068-9', '111book', NULL, NULL),
(892, '0-596-52068-9', '110book', NULL, NULL),
(893, '0-596-52068-9', '109book', NULL, NULL),
(894, '0-596-52068-9', '108book', NULL, NULL),
(895, '0-596-52068-9', '107book', NULL, NULL),
(896, '0-596-52068-9', '106book', NULL, NULL),
(897, '0-596-52068-9', '105book', NULL, NULL),
(898, '0-596-52068-9', '104book', NULL, NULL),
(899, '0-596-52068-9', '103book', NULL, NULL),
(900, '0-596-52068-9', '102book', NULL, NULL),
(901, '0-596-52068-9', '101book', NULL, NULL),
(902, '0-596-52068-9', '100book', NULL, NULL),
(903, '0-596-52068-9', '99book', NULL, NULL),
(904, '0-596-52068-9', '98book', NULL, NULL),
(905, '0-596-52068-9', '97book', NULL, NULL),
(906, '0-596-52068-9', '96book', NULL, NULL),
(907, '0-596-52068-9', '95book', NULL, NULL),
(908, '0-596-52068-9', '94book', NULL, NULL),
(909, '0-596-52068-9', '93book', NULL, NULL),
(910, '0-596-52068-9', '92book', NULL, NULL),
(911, '0-596-52068-9', '91book', NULL, NULL),
(912, '0-596-52068-9', '90book', NULL, NULL),
(913, '0-596-52068-9', '89book', NULL, NULL),
(914, '0-596-52068-9', '88book', NULL, NULL),
(915, '0-596-52068-9', '87book', NULL, NULL),
(916, '0-596-52068-9', '86book', NULL, NULL),
(917, '0-596-52068-9', '85book', NULL, NULL),
(918, '0-596-52068-9', '84book', NULL, NULL),
(919, '0-596-52068-9', '83book', NULL, NULL),
(920, '0-596-52068-9', '82book', NULL, NULL),
(921, '0-596-52068-9', '81book', NULL, NULL),
(922, '0-596-52068-9', '80book', NULL, NULL),
(923, '0-596-52068-9', '79book', NULL, NULL),
(924, '0-596-52068-9', '78book', NULL, NULL),
(925, '0-596-52068-9', '77book', NULL, NULL),
(926, '0-596-52068-9', '76book', NULL, NULL),
(927, '0-596-52068-9', '75book', NULL, NULL),
(928, '0-596-52068-9', '74book', NULL, NULL),
(929, '0-596-52068-9', '73book', NULL, NULL),
(930, '0-596-52068-9', '72book', NULL, NULL),
(931, '0-596-52068-9', '71book', NULL, NULL),
(932, '0-596-52068-9', '70book', NULL, NULL),
(933, '0-596-52068-9', '69book', NULL, NULL),
(934, '0-596-52068-9', '68book', NULL, NULL),
(935, '0-596-52068-9', '67book', NULL, NULL),
(936, '0-596-52068-9', '66book', NULL, NULL),
(937, '0-596-52068-9', '65book', NULL, NULL),
(938, '0-596-52068-9', '64book', NULL, NULL),
(939, '0-596-52068-9', '63book', NULL, NULL),
(940, '0-596-52068-9', '62book', NULL, NULL),
(941, '0-596-52068-9', '61book', NULL, NULL),
(942, '0-596-52068-9', '60book', NULL, NULL),
(943, '0-596-52068-9', '59book', NULL, NULL),
(944, '0-596-52068-9', '58book', NULL, NULL),
(945, '0-596-52068-9', '57book', NULL, NULL),
(946, '0-596-52068-9', '56book', NULL, NULL),
(947, '0-596-52068-9', '55book', NULL, NULL),
(948, '0-596-52068-9', '54book', NULL, NULL),
(949, '0-596-52068-9', '53book', NULL, NULL),
(950, '0-596-52068-9', '52book', NULL, NULL),
(951, '0-596-52068-9', '51book', NULL, NULL),
(952, '0-596-52068-9', '50book', NULL, NULL),
(953, '0-596-52068-9', '49book', NULL, NULL),
(954, '0-596-52068-9', '48book', NULL, NULL),
(955, '0-596-52068-9', '47book', NULL, NULL),
(956, '0-596-52068-9', '46book', NULL, NULL),
(957, '0-596-52068-9', '45book', NULL, NULL),
(958, '0-596-52068-9', '44book', NULL, NULL),
(959, '0-596-52068-9', '43book', NULL, NULL),
(960, '0-596-52068-9', '42book', NULL, NULL),
(961, '0-596-52068-9', '41book', NULL, NULL),
(962, '0-596-52068-9', '40book', NULL, NULL),
(963, '0-596-52068-9', '39book', NULL, NULL),
(964, '0-596-52068-9', '38book', NULL, NULL),
(965, '0-596-52068-9', '37book', NULL, NULL),
(966, '0-596-52068-9', '36book', NULL, NULL),
(967, '0-596-52068-9', '35book', NULL, NULL),
(968, '0-596-52068-9', '34book', NULL, NULL),
(969, '0-596-52068-9', '33book', NULL, NULL),
(970, '0-596-52068-9', '32book', NULL, NULL),
(971, '0-596-52068-9', '31book', NULL, NULL),
(972, '0-596-52068-9', '30book', NULL, NULL),
(973, '0-596-52068-9', '29book', NULL, NULL),
(974, '0-596-52068-9', '28book', NULL, NULL),
(975, '0-596-52068-9', '27book', NULL, NULL),
(976, '0-596-52068-9', '26book', NULL, NULL),
(977, '0-596-52068-9', '25book', NULL, NULL),
(978, '0-596-52068-9', '24book', NULL, NULL),
(979, '0-596-52068-9', '23book', NULL, NULL),
(980, '0-596-52068-9', '22book', NULL, NULL),
(981, '0-596-52068-9', '21book', NULL, NULL),
(982, '0-596-52068-9', '20book', NULL, NULL),
(983, '0-596-52068-9', '19book', NULL, NULL),
(984, '0-596-52068-9', '18book', NULL, NULL),
(985, '0-596-52068-9', '17book', NULL, NULL),
(986, '0-596-52068-9', '16book', NULL, NULL),
(987, '0-596-52068-9', '15book', NULL, NULL),
(988, '0-596-52068-9', '14book', NULL, NULL),
(989, '0-596-52068-9', '13book', NULL, NULL),
(990, '0-596-52068-9', '12book', NULL, NULL),
(991, '0-596-52068-9', '11book', NULL, NULL),
(992, '0-596-52068-9', '10book', NULL, NULL),
(993, '0-596-52068-9', '9book', NULL, NULL),
(994, '0-596-52068-9', '8book', NULL, NULL),
(995, '0-596-52068-9', '7book', NULL, NULL),
(996, '0-596-52068-9', '6book', NULL, NULL),
(997, '0-596-52068-9', '5book', NULL, NULL),
(998, '0-596-52068-9', '4book', NULL, NULL),
(999, '0-596-52068-9', '3book', NULL, NULL),
(1000, '0-596-52068-9', '2book', NULL, NULL),
(1001, '0-596-52068-9', '1book', NULL, NULL),
(1002, '0-596-52068-9', '0book', NULL, NULL),
(1003, '0-596-52068-9', 'my new book', NULL, 'my new book 0-596-52068-9 '),
(1004, '0-596-52068-9', 'my book', NULL, 'my book 0-596-52068-9 '),
(1005, '0-596-52068-9', 'Eto moja novaja', NULL, 'Eto moja novaja 0-596-52068-9 '),
(1006, '0-596-52068-9', 'new interesting book', NULL, 'new interesting book 0-596-52068-9 ');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`) VALUES
(1, 'ROLE_ACTUATOR'),
(2, 'ROLE_ADMIN'),
(3, 'ROLE_USER');

-- --------------------------------------------------------

--
-- Table structure for table `side_menu`
--

CREATE TABLE `side_menu` (
  `id` bigint(20) NOT NULL,
  `attr_name` varchar(255) DEFAULT NULL,
  `attr_value` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `sort` int(11) NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `top_menu_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `side_menu`
--

INSERT INTO `side_menu` (`id`, `attr_name`, `attr_value`, `name`, `sort`, `url`, `top_menu_id`) VALUES
(1, NULL, NULL, 'Books', 1, '/books', 1);

-- --------------------------------------------------------

--
-- Table structure for table `top_menu`
--

CREATE TABLE `top_menu` (
  `id` bigint(20) NOT NULL,
  `attr_name` varchar(255) DEFAULT NULL,
  `attr_value` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `sort` int(11) NOT NULL,
  `url` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `top_menu`
--

INSERT INTO `top_menu` (`id`, `attr_name`, `attr_value`, `name`, `sort`, `url`) VALUES
(1, NULL, NULL, 'Welcome', 1, '/');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL,
  `is_account_non_expired` bit(1) DEFAULT NULL,
  `is_account_non_locked` bit(1) DEFAULT NULL,
  `is_credentials_non_expired` bit(1) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `is_enabled` bit(1) DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `password_expire` datetime DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `is_account_non_expired`, `is_account_non_locked`, `is_credentials_non_expired`, `email`, `is_enabled`, `firstname`, `lastname`, `password`, `password_expire`, `phone`) VALUES
(1, b'1', b'1', b'1', 'tieto@tieto.com', b'1', 'Jurijs', 'Grigorjevs', '$2a$10$beyK5t6XrDBHEuHxMiUjrujVlCAfj1MOcHKr3VwGzitGcBPzo5PSC', '2018-12-31 00:00:00', '26043578');

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `user_id` bigint(20) NOT NULL,
  `role_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`user_id`, `role_id`) VALUES
(1, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UKhvrbuk2451gcnwthweo2r04ki` (`book_name`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `side_menu`
--
ALTER TABLE `side_menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKb5bikmwn1fhacww0f2b0ao9l1` (`top_menu_id`);

--
-- Indexes for table `top_menu`
--
ALTER TABLE `top_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK6dotkott2kjsp8vw4d0m25fb7` (`email`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `FKt7e7djp752sqn6w22i6ocqy6q` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1007;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `side_menu`
--
ALTER TABLE `side_menu`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `top_menu`
--
ALTER TABLE `top_menu`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `side_menu`
--
ALTER TABLE `side_menu`
  ADD CONSTRAINT `FKb5bikmwn1fhacww0f2b0ao9l1` FOREIGN KEY (`top_menu_id`) REFERENCES `top_menu` (`id`);

--
-- Constraints for table `user_role`
--
ALTER TABLE `user_role`
  ADD CONSTRAINT `FKj345gk1bovqvfame88rcx7yyx` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `FKt7e7djp752sqn6w22i6ocqy6q` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
